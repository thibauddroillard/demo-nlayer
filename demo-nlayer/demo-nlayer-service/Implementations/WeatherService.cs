﻿using demo_nlayer_service.Interfaces;
using demo_nlayer_repository.Interfaces;
using demo_nlayer_repository.Models;

namespace demo_nlayer_service.Implementations
{
    public class WeatherService : IWeatherService
    {
        private IWeatherRepository _weatherRepository;

        public WeatherService(IWeatherRepository weatherRepository)
        {
            _weatherRepository = weatherRepository;
        }

        /**
         * Récupération de la liste des weathers
         */
        public async Task<IList<WeatherDTO>> GetAllWeather()
        {
            // logique metier

            // appel au repository pour accèder aux données
            return await _weatherRepository.GetAllWeather();
        }
    }
}

