﻿using demo_nlayer_repository.Models;

namespace demo_nlayer_service.Interfaces
{
    public interface IWeatherService
    {
        // liste des méthodes de l'implémentation du service
        Task<IList<WeatherDTO>> GetAllWeather();
    }
}

