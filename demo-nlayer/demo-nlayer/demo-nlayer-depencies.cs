﻿using demo_nlayer_repository.Implementations;
using demo_nlayer_repository.Interfaces;
using demo_nlayer_service.Implementations;
using demo_nlayer_service.Interfaces;

namespace demo_nlayer
{
    public static class demo_nlayer_depencies
    {
        /**
         * Retourne une liste de scopes pour l'injection de dépendances des interfaces
         */
        public static IServiceCollection AddDemoNLayerDependencies(this IServiceCollection serviceCollection)
        {
            // ajouter la liste des couples interfaces / implémentations pour l'injection de dépendances
            return serviceCollection
                .AddScoped<IWeatherRepository, WeatherRepository>()
                .AddScoped<IWeatherService, WeatherService>();
        }
    }
}

