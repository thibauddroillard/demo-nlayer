﻿using System;
using demo_nlayer.ViewModels;
using demo_nlayer_repository.Models;

namespace demo_nlayer.Mappers
{
	public static class WeatherMapper
	{
		/**
		 * conversion du DTO vers le view model
		 */
		public static WeatherDTO toWeatherForecastDTO(this WeatherViewModel weatherViewModel)
		{
			return new WeatherDTO()
			{
				Date = weatherViewModel.Date,
				TemperatureC = weatherViewModel.TemperatureC,
				Summary = weatherViewModel.Summary
			};
		}

		/**
		 * conversion du DTO vers le view model
		 */
        public static WeatherViewModel toWeatherForecastViewModel(this WeatherDTO weatherDTO)
        {
            return new WeatherViewModel()
            {
                Date = weatherDTO.Date,
                TemperatureC = weatherDTO.TemperatureC,
                Summary = weatherDTO.Summary
            };
        }
    }
}

