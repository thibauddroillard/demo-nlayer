﻿using demo_nlayer.Mappers;
using demo_nlayer.ViewModels;
using demo_nlayer_service.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace demo_nlayer.Controllers;

[ApiController]
[Route("[controller]")]
public class WeatherForecastController : ControllerBase
{
    private readonly IWeatherService _weatherService;
    private readonly ILogger<WeatherForecastController> _logger;

    public WeatherForecastController(IWeatherService weatherService, ILogger<WeatherForecastController> logger)
    {
        _logger = logger;
        _weatherService = weatherService;
    }

    /**
     * Récupération de tous les Weather
     */
    [HttpGet(Name = "GetWeatherForecast")]
    public async Task<ActionResult<IList<WeatherViewModel>>> Get()
    {
        // récupération des données via le service
        var weatherDTOs = await _weatherService.GetAllWeather();

        // si on ne trouve pas de données on log l'erreur et retourne une erreur 404
        if (weatherDTOs is null)
        {
            _logger.LogWarning("Aucune weather trouvé");
            return NotFound();
        }

        // conversion du DTO vers un ViewModel
        var weatherViewModels = weatherDTOs.Select(weather => weather.toWeatherForecastViewModel()).ToList();

        // retourne code 200 avec le résultat
        return Ok(weatherViewModels);
    }
}

