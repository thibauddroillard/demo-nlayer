﻿using demo_nlayer_repository.Models;

namespace demo_nlayer_repository.Interfaces
{
    public interface IWeatherRepository
    {
        // liste des méthodes de l'implémentation du repository
        Task<IList<WeatherDTO>> GetAllWeather();
    }
}

