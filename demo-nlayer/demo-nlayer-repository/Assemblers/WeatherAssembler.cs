﻿using demo_nlayer_data.Models;
using demo_nlayer_repository.Models;

namespace demo_nlayer_repository.Assemblers
{
    public static class WeatherAssembler
    {
        /**
         * conversion du DTO vers le model
         */
        public static WeatherDTO toWeatherForecastDTO(this Weather weather)
        {
            return new WeatherDTO()
            {
                Date = weather.Date,
                TemperatureC = weather.TemperatureC,
                Summary = weather.Summary
            };
        }

        /**
         * conversion du model vers le DTO
         */
        public static Weather toWeatherForecast(this WeatherDTO weatherDTO)
        {
            return new Weather()
            {
                Date = weatherDTO.Date,
                TemperatureC = weatherDTO.TemperatureC,
                Summary = weatherDTO.Summary
            };
        }
    }
}

