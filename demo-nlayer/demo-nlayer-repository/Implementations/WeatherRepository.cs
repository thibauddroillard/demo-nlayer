﻿using demo_nlayer_data.Models;
using demo_nlayer_repository.Assemblers;
using demo_nlayer_repository.Interfaces;
using demo_nlayer_repository.Models;

namespace demo_nlayer_repository.Implementations
{
    public class WeatherRepository : IWeatherRepository
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        public WeatherRepository()
        {
        }

        /**
         * Récupération de la liste des weathers
         */
        public async Task<IList<WeatherDTO>> GetAllWeather()
        {
            // récupération des données 
            var weathers = Enumerable.Range(1, 5).Select(index => new Weather
            {
                Date = DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            })
            .ToList();

            // conversion des données vers un DTO
            return weathers.Select(weather => weather.toWeatherForecastDTO()).ToList();
        }
    }
}
