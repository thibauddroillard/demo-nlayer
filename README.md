# demo-NLayer

![n-layer.svg](./n-layer.svg)

### Controller
Gére les appels API et la gestion des permissions

- ViewModel → entité retournée par le “Controller”
- Mapper →  transforme les DTO en ViewModel
- Dependencies → Liste des dépendances entre interfaces et implémentation

### Service

Contient la logique métier
- calcule
- manipulation d’objet

- Manipule des DTO

### Repository

Permet l'interaction avec la base de données ou avec d'autre API
 - lecture
 - écriture

- DTO → entité retournée par les “Repository”
- Assembler → transforme les “Models” en DTO

### Migration

- Model → entité lu ou écrite par le “Repository”
